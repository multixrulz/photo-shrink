#!/usr/bin/python
"""
Code used in just about every part of photo-shrink.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from PyQt5 import QtWidgets
import os
from enum import Enum
# Imports from photo-shrink
from . import build_config

class UiFile(Enum):
    """
    An enumeration used to easily and safely refer to specify the Qt .ui
    files used in the application.

    Use the filename() method to get the canonical filename of a particular
    .ui file.
    """
    MainWindow = 'ps_MainWindow.ui'
    Progress = 'ps_Progress.ui'

    def __str__(self):
        # Need to be able to get an actual string for each record type
        return self.value

    def filename(self):
        """
        Get the canonical filename of the .ui file.
        """
        return os.path.join(build_config.UI_DIR, self.value)
