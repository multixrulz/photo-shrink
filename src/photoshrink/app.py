#!/usr/bin/python
"""
A convenience module for starting the photo-shrink program.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtWidgets, QtGui
import sys
import os
# Imports from photo-shrink
from . import MainWindowUi, build_config

def exec():
    """
    Execute the photo-resize program.

    Simply import this module and execute this exec() function.
    """
    # Create an application object
    app = QtWidgets.QApplication(sys.argv)

    # Create a splash screen, as connections to the database may take a little while
    #pixmap = QtGui.QPixmap(os.path.join(build_config.PIXMAP_DIR, 'takestock-splash.png'))
    #splash = QtWidgets.QSplashScreen(pixmap)
    #splash.show()
    #app.processEvents()

    # Set an icon for the application
    app_icon = QtGui.QIcon(os.path.join(build_config.PIXMAP_DIR, 'photo-shrink.svg'))
    app.setWindowIcon(app_icon)

    # TODO: Parse the commandline better
    if len(sys.argv) >= 2:
        config.set_profile(sys.argv[1])

    # Create and show the main window -- the window shows itself
    main_window = MainWindowUi.photoshrinkMainWindow(app)

    # Close the splash screen when the window is shown
    #splash.finish(main_window)

    # Execute the application -- get the event loop running
    return app.exec()
