#!/bin/bash

echo "Setting correct permissions on ui files"
chmod 644 src/ui/*.ui
#echo "Setting correct permissions on other files"
chmod 644 src/photoshrink/*.py
chmod 644 src/pixmaps/*
chmod 644 src/photo-shrink.desktop
python setup.py sdist
