#!/usr/bin/python
"""
photo-shrink configuration.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser
import os
import xdg.BaseDirectory
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
# Imports from photo-shrink
from . import build_config

class _Config():
    # Application defaults
    app_defaults = {'photo-shrink': { # [Photo-shrink] section if config file
        'output_dir': "~/shrunk",
        'constraint': "File size",
        'quality': "Low"}
        }

    ### Holds and allows editing of configuration information.
    def __init__(self):
        # Path to config file
        self._config_filename = os.path.join(
            xdg.BaseDirectory.save_config_path('photo-shrink'),
            'photo-shrink.conf')

        # Create a configparser object, initialise it with defaults,
        # and read in the config file
        self._cp = configparser.ConfigParser(delimiters=('='))
        self._cp.read_dict(self.app_defaults)
        self._cp.read(self._config_filename)

    def save(self):
        with open(self._config_filename, 'w') as configfile:
            self._cp.write(configfile)

_config = _Config()

def output_dir():
    """
    Get the directory to write shrunk files into.
    """
    return os.path.expanduser(_config._cp.get('photo-shrink', 'output_dir'))

def set_output_dir(path):
    """
    Set the directory to write shrunk files into.
    """
    return _config._cp.set('photo-shrink', 'output_dir', path)

def quality():
    """
    Get the selected quality.
    """
    return _config._cp.get('photo-shrink', 'quality')

def set_quality(quality):
    """
    Set the selected quality.

    quality: The quality desired.
    """
    return _config._cp.set('photo-shrink', 'quality', quality)

def constraint():
    """
    Get the selected constraint.
    """
    return _config._cp.get('photo-shrink', 'constraint')

def set_constraint(constraint):
    """
    Set the selected constraint.

    constraint:  the variable being constrained.
    """
    return _config._cp.set('photo-shrink', 'constraint', constraint)

def constrained_variable(constraint):
    """
    Get the selected constrained variable.

    constraint: the constraint being configured.
    """
    try:
        constrained = _config._cp.get('photo-shrink', constraint)
    except configparser.NoOptionError:
        # This constraint hasn't been configured before.
        return None
    return constrained

def set_constrained_variable(constraint, constrained_variable):
    """
    Set the selected constrained variable.

    constraint: the constraint being configured.
    constrained_variable: the value of the constraint.
    """
    return _config._cp.set('photo-shrink', constraint, constrained_variable)

def save():
    _config.save()
