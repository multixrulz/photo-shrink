#!/usr/bin/python
"""Customised Qt classes for use with photo-shrink."""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtCore, QtGui, QtWidgets


class ClassConvertor():
    """
    A class that only converts other classes.
    """
    @classmethod
    def morph(cls, obj):
        """
        Convert an object of some class to this class.
        """
        obj.__class__ = cls
        obj._morph_instance()


class psListWidget(QtWidgets.QListWidget, ClassConvertor):
    def __init__(self, *args, **kwargs):
        """
        Create the widget.
        """
        super().__init__(*args, **kwargs)
        self._morph_instance()

    def _morph_instance(self):
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setDragEnabled(False)
        self.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.setDefaultDropAction(QtCore.Qt.CopyAction)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            for url in event.mimeData().urls():
                if not self.findItems(str(url.toLocalFile()), QtCore.Qt.MatchContains):
                    self.addItem(str(url.toLocalFile()))
        else:
            event.ignore()
