#!/usr/bin/python
"""
The photo-shrink main window GUI class.
"""

#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5 import uic
import sys
import subprocess
import traceback # For exception handler.
# Imports from photo-shrink
from .QtExtensions import *
from .common import *
from . import config


class photoshrinkMainWindow(QtWidgets.QMainWindow):
    """The TakeStock main window."""
    def __init__(self, app, *args, **kwargs):
        """
        Create the window.
        """
        self.app = app

        # Set a custom exception hook in order to display a nice message
        # dialog when an exception occurs.
        sys.excepthook = self._custom_exception_hook

        super().__init__(*args, **kwargs)

        # Load the UI
        uic.loadUi(UiFile.MainWindow.filename(), self)

        # Initialisations
        self.lineEdit_dest_dir.setText(config.output_dir())
        self.progressBar.hide()
        self._initialise_constraint()
        self._initialise_constrained()
        self._initialise_quality()

        # Conversions
        psListWidget.morph(self.listWidget_input_files)

        # Add actions to buttons - this allows them to work
        self.pb_exit.addAction(self.actionExit)

        self.show()

    def _initialise_quality(self):
        qualities = ['Poor', 'Low', 'OK']
        # Disconnect the signal before adding new items, to prevent it from firing
        # and making unwanted changes to the config.
        self.comboBox_quality.currentTextChanged.disconnect()
        self.comboBox_quality.insertItems(0, qualities)
        self.comboBox_quality.currentTextChanged.connect(self._quality_changed)
        self.comboBox_quality.setCurrentText(config.quality())

    def _initialise_constraint(self):
        constraints = ['File size', '4:3 display', '16:9 display',
            'Other display', 'Longest side']
        # Disconnect the signal before adding new items, to prevent it from firing
        # and making unwanted changes to the config.
        self.comboBox_constraint.currentTextChanged.disconnect()
        self.comboBox_constraint.insertItems(0, constraints)
        self.comboBox_constraint.currentTextChanged.connect(self._constraint_changed)
        self.comboBox_constraint.setCurrentText(config.constraint())

    def _initialise_constrained(self):
        constrained_options = {"File size": ['50kB', '100kB', '250kB', '500kB', '1MB'],
            "4:3 display": ['640x480', '800x600', '1024x768', '1280x960', '1440x1050',
                '1600x1200', '2048x1536'],
            "16:9 display": ['1024x576', '1280x720', '1366x768', '1920x1080', '3840x2160'],
            "Other display": ['1280x800', '1440x900', '1920x1200', '2048x1080',
                '4096x2160'],
            "Longest side": ['500', '800', '1200', '2000', '3500', '5000']}
        constraint = self.comboBox_constraint.currentText()
        # Disconnect the signal before adding new items, to prevent it from firing
        # and making unwanted changes to the config.
        self.comboBox_constrained.currentTextChanged.disconnect()
        self.comboBox_constrained.clear()
        self.comboBox_constrained.insertItems(0, constrained_options[constraint])
        constrained = config.constrained_variable(constraint)
        if constrained is not None:
            self.comboBox_constrained.setCurrentText(constrained)
        self.comboBox_constrained.currentTextChanged.connect(self._constrained_changed)

    def _quality_changed(self, new_value):
        config.set_quality(new_value)
        config.save()

    def _constraint_changed(self, new_value):
        config.set_constraint(new_value)
        config.save()
        self._initialise_constrained()
        constrained = self.comboBox_constrained.currentText()
        config.set_constrained_variable(new_value, constrained)
        config.save()

    def _constrained_changed(self, new_value):
        constraint = self.comboBox_constraint.currentText()
        config.set_constrained_variable(constraint, new_value)
        config.save()

    def _pb_dest_dir_clicked(self):
        dialog = QtWidgets.QFileDialog()
        dialog.setDirectory(os.path.expanduser('~'))
        dialog.setFileMode(QtWidgets.QFileDialog.DirectoryOnly)
        dialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        dialog.setOption(QtWidgets.QFileDialog.ShowDirsOnly, True)
        if dialog.exec():
            filenames = dialog.selectedFiles()
            self.lineEdit_dest_dir.setText(filenames[0])
            config.set_output_dir(filenames[0])

    def _pb_shrink_clicked(self):
        # Get the destination directory and ensure it exists.
        dest_dir = self.lineEdit_dest_dir.text()
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)
        cmd_array = []

        # Get the constraint and process accordingly
        constraint = self.comboBox_constraint.currentText()
        quality = self.comboBox_quality.currentText()
        if constraint == "File size":
            filesize = self.comboBox_constrained.currentText()
            if quality == "Poor":
                res_lookup = {'50kB': '800',
                    '100kB': '1200',
                    '250kB': '2000',
                    '500kB': '3500',
                    '1MB': '5000'}
            elif quality == "Low":
                res_lookup = {'50kB': '600',
                    '100kB': '900',
                    '250kB': '1500',
                    '500kB': '2000',
                    '1MB': '3500'}
            elif quality == "OK":
                res_lookup = {'50kB': '400',
                    '100kB': '600',
                    '250kB': '1000',
                    '500kB': '1750',
                    '1MB': '2500'}
            resolution = res_lookup[filesize]
            cmd_array.extend(['-resize', '%sx%s' % (resolution, resolution)])
        elif (constraint == "4:3 display" or constraint == "16:9 display"
            or constraint == "Other display"):
            resolution = self.comboBox_constrained.currentText()
            if quality == "Poor":
                filesize_lookup = {'640x480': '40kB',
                    '800x600': '50kB',
                    '1024x768': '80kB',
                    '1280x960': '100kB',
                    '1440x1050': '140kB',
                    '1600x1200': '180kB',
                    '2048x1536': '250kB',
                    '1024x576': '70kB',
                    '1280x720': '90kB',
                    '1366x768': '130kB',
                    '1920x1080': '220kB',
                    '3840x2160': '500kB',
                    '1280x800': '75kB',
                    '1440x900': '130kB',
                    '1920x1200': '230kB',
                    '2048x1080': '225kB',
                    '4096x2160': '800kB'}
            elif quality == "Low":
                filesize_lookup = {'640x480': '50kB',
                    '800x600': '80kB',
                    '1024x768': '100kB',
                    '1280x960': '140kB',
                    '1440x1050': '180kB',
                    '1600x1200': '250kB',
                    '2048x1536': '320kB',
                    '1024x576': '90kB',
                    '1280x720': '130kB',
                    '1366x768': '220kB',
                    '1920x1080': '500kB',
                    '3840x2160': '1000kB',
                    '1280x800': '130kB',
                    '1440x900': '230kB',
                    '1920x1200': '400kB',
                    '2048x1080': '400kB',
                    '4096x2160': '1200kB'}
            elif quality == "OK":
                filesize_lookup = {'640x480': '80kB',
                    '800x600': '100kB',
                    '1024x768': '140kB',
                    '1280x960': '180kB',
                    '1440x1050': '250kB',
                    '1600x1200': '320kB',
                    '2048x1536': '450kB',
                    '1024x576': '130kB',
                    '1280x720': '220kB',
                    '1366x768': '300kB',
                    '1920x1080': '700kB',
                    '3840x2160': '1400kB',
                    '1280x800': '200kB',
                    '1440x900': '320kB',
                    '1920x1200': '500kB',
                    '2048x1080': '500kB',
                    '4096x2160': '2MB'}
            filesize = filesize_lookup[resolution]
            cmd_array.extend(['-resize', '%s' % (resolution)])
        elif constraint == "Longest side":
            resolution = self.comboBox_constrained.currentText()
            if quality == "Poor":
                filesize_lookup = {'500': '35kB',
                    '800': '50kB',
                    '1200': '100kB',
                    '2000': '250kB',
                    '3500': '500kB',
                    '5000': '1MB'}
            elif quality == "Low":
                filesize_lookup = {'500': '50kB',
                    '800': '100kB',
                    '1200': '250kB',
                    '2000': '500kB',
                    '3500': '1MB',
                    '5000': '2MB'}
            elif quality == "OK":
                filesize_lookup = {'500': '100kB',
                    '800': '250kB',
                    '1200': '500kB',
                    '2000': '1MB',
                    '3500': '2MB',
                    '5000': '4MB'}
            filesize = filesize_lookup[resolution]
            cmd_array.extend(['-resize', '%sx%s' % (resolution, resolution)])
        cmd_array.extend(['-define', 'jpeg:extent=%s' % filesize,
            '-define', 'jpeg:optimize-coding=on', '-alpha', 'remove', '-background', '#ffffff', '-strip'])
        # Show a progress bar
        self.progressBar.show()
        self.progressBar.setValue(0)
        # Iterate through the list
        photo_count = self.listWidget_input_files.count()
        for x in range(photo_count):
            # Update the progress bar
            progress = 100*x/photo_count
            self.progressBar.setValue(progress)
            # Do the conversion
            item = self.listWidget_input_files.item(x)
            source = item.data(QtCore.Qt.DisplayRole)
            dest = os.path.join(dest_dir, "%s.jpg" % os.path.splitext(os.path.basename(source))[0])
            command = ['convert', source] + cmd_array + [dest]
            print(' '.join(command))
            subprocess.call(command)
        # Update the progress bar
        progress = 100
        self.progressBar.setValue(progress)
        self.progressBar.hide()
        # Delete all files from the list
        self.listWidget_input_files.clear()

    def _pb_exit_clicked(self):
        config.save()
        self.app.exit()

    def _custom_exception_hook(self, type, value, actual_traceback):
        ### Define a customised sys.excepthook to display exceptions nicely to the user
        traceback_string = "".join(traceback.format_exception(type, value, actual_traceback))
        sys.stderr.write(traceback_string)
        sys.stderr.flush()
        # TODO: TRANSLATE
        primary_text = "Oops! We seem to have hit a bug."
        secondary_text = "Photo-shrink will now close.  The text below will be helpful to the developers."
        exit_app = True

        # Display the error in a message box
        msgbox = QtWidgets.QMessageBox()
        msgbox.setWindowTitle("Photo-shrink Error")
        msgbox.setIcon(QtWidgets.QMessageBox.Critical)
        msgbox.setText("<h1>%s</h1>%s" % (primary_text, secondary_text))
        msgbox.setDetailedText(traceback_string)
        msgbox.setStandardButtons(QtWidgets.QMessageBox.Close)
        msgbox.setDefaultButton(QtWidgets.QMessageBox.Close)
        result = msgbox.exec()
        if exit_app:
            self.app.quit()


if __name__ == '__main__':
    print("You can't run photo-shrink this way.  Try the following instead:\nimport photoresize.app\n\nphotoresize.app.exec()")
