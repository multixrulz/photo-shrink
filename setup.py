#!/usr/bin/env python

import distutils.command.build_py
import glob
import os
import sys

# NOTE: distutils takes care of pathname separators, expecting everything
# to use slashes.  But when glob is used, os.path.join must be used too.

PACKAGE_DIR = 'src/photoshrink'
UI_FILES =  glob.glob(os.path.join('src', 'ui', '*.ui'))

class build_py(distutils.command.build_py.build_py):
    """Modified build_py class"""
    def build_module (self, module, module_file, package):
        """
        Copied directly from TakeStock.
        """
        # Get the install prefix
        try:
            iobj = self.distribution.command_obj['install']
            prefix = iobj.prefix
        except KeyError as e:
            print (e)
            prefix = sys.prefix
        # Specify the path to the ui files
        ui_dir = os.path.join(prefix, 'share', 'photo-shrink', 'ui')
        pixmap_dir = os.path.join(prefix, 'share', 'pixmaps', 'photo-shrink')

        # Modify the build_config.py file that contains this path definition
        if module_file == os.path.join(PACKAGE_DIR, 'build_config.py'):
            # Read in build_config.py
            with open(module_file) as mf:
                lines = mf.read().splitlines()

            # Find the line starting 'UI_DIR = ' and overwrite it
            for i, line in enumerate(lines):
                if line.startswith("UI_DIR = "):
                    lines[i] = "UI_DIR = '%s'" % (ui_dir)
                if line.startswith("PIXMAP_DIR = "):
                    lines[i] = "PIXMAP_DIR = '%s'" % (pixmap_dir)

            # Write the file out again
            with open(module_file, 'w') as mf:
                mf.write("\n".join(lines))
        # Execute the original build_module() method.
        return distutils.command.build_py.build_py.build_module(self, module, module_file, package)

distutils.core.setup(name='photo-shrink',
    version='1.1',
    description='Shrink photo filesizes for email or web use, etc.',
    url='http://jared.henley.id.au/software/photo-shrink',
    author='Jared Henley',
    author_email='jared@henley.id.au',
    packages=['photoshrink'],
    package_dir={'photoshrink': PACKAGE_DIR},
    data_files=[('share/photo-shrink/ui', UI_FILES),
        ('share/doc/photo-shrink', ['src/COPYING',]),
        #('share/pixmaps/TakeStock', ['src/pixmaps/takestock-splash.png',]),
        ('share/pixmaps', ['src/pixmaps/photo-shrink.png', 'src/pixmaps/photo-shrink.svg']),
        ('share/applications', ['src/photo-shrink.desktop',])
        ],
    scripts=['src/photo-shrink'],
    cmdclass={"build_py": build_py
    }
    )
